# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import tornado.web
import tornado.gen
import tornado.ioloop
import tornado.template
import tornado.httpclient

import os
import json
import gzip
import signal
import datetime
import argparse
import traceback
import email.utils

import fdroidmonitor
import fdroidmonitor.cfg


AF_NAMES = {
    "Ads": "Advertising",
    "Tracking": "Tracking",
    "NonFreeNet": "Non-Free Network Services",
    "NonFreeAdd": "Non-Free Addons",
    "NonFreeDep": "Non-Free Dependencies",
    "UpstreamNonFree": "Upstream Non-Free",
    "NonFreeAssets": "Non-Free Assets",
    "KnownVuln": "Known Vulnerability",
    "DisabledAlgorithm": "Disabled Algorithm",
    "NoSourceSince": "No Source Since",
}


def template_path(template):
    return os.path.join(
        os.path.abspath(os.path.dirname(__file__)), "templates", template
    )


def parse_http_datetime(rawdate):
    return datetime.datetime(*email.utils.parsedate(rawdate)[:6])


async def load_update():
    http_client = tornado.httpclient.AsyncHTTPClient()
    response = await http_client.fetch(
        "https://f-droid.org/repo/status/update.json",
        connect_timeout=120,
        request_timeout=120,
    )
    if response.error:
        raise response.error
    last_modified = parse_http_datetime(response.headers["last-modified"])
    d = json.loads(response.body)
    data = {
        "data": d,
        "lastModified": last_modified,
        "startTime": datetime.datetime.utcfromtimestamp(d["startTimestamp"] / 1000),
        "endTime": datetime.datetime.utcfromtimestamp(d["endTimestamp"] / 1000),
    }
    return data


class MyException(Exception):
    def __init__(self, msg="", desc="", status=500):
        super().__init__(msg)
        self.desc = desc
        self.status = status


class BaseRequestHandler(tornado.web.RequestHandler):
    def get_template_namespace(self):
        namespace = super().get_template_namespace()
        cfg = fdroidmonitor.cfg.load()
        namespace.update({"monitorVersion": fdroidmonitor.VERSION, "cfg": cfg})
        return namespace

    # def on_finish(self):
    #     cfg = fdroidmonitor.cfg.load()
    #     if cfg.get("debug", None):
    #       print(self.request.method, self.get_status(), self.request.path)

    def write_error(self, status_code, **kwargs):
        ex = kwargs["exc_info"][1]
        tb = kwargs["exc_info"][2]
        data = {"err_title": str(ex), "err_text": "", "traceback": ""}
        cfg = fdroidmonitor.cfg.load()
        if cfg.get("debug", ""):
            data["traceback"] = "".join(traceback.format_exception(None, ex, tb))
        if isinstance(ex, MyException):
            data["err_text"] = ex.desc
            self.set_status(ex.status)
        self.render(template_path("err.html"), **data)


class Err404Handler(BaseRequestHandler):
    def prepare(self):
        data = {
            "err_title": "HTTP 404: Not Found",
            "err_text": f"{self.request.path} not found",
            "traceback": "",
        }
        self.set_status(404)
        self.render(template_path("err.html"), **data)


class IndexHandler(BaseRequestHandler):
    def get(self):
        self.redirect("/builds")


class BuildsHandler(BaseRequestHandler):
    def get(self):
        data = {"title": "This is TITLE"}
        self.render(template_path("builds.html"), **data)


class RunningHandler(BaseRequestHandler):
    async def get(self):
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            "https://f-droid.org/repo/status/running.json",
            connect_timeout=120,
            request_timeout=120,
        )
        last_modified = parse_http_datetime(response.headers["last-modified"])
        d = json.loads(response.body)
        data = {
            "data": d,
            "startTime": datetime.datetime.utcfromtimestamp(d["startTimestamp"] / 1000),
            "lastModified": last_modified,
        }
        self.render(template_path("running.html"), **data)


class BuildHandler(BaseRequestHandler):
    async def get(self):
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            "https://f-droid.org/repo/status/build.json",
            connect_timeout=120,
            request_timeout=120,
        )
        last_modified = parse_http_datetime(response.headers["last-modified"])
        d = json.loads(response.body)
        data = {
            "cmd": " ".join(d["commandLine"]),
            "startTime": datetime.datetime.utcfromtimestamp(d["startTimestamp"] / 1000),
            "endTime": datetime.datetime.utcfromtimestamp(d["endTimestamp"] / 1000),
            "data": d,
            "lastModified": last_modified,
        }

        self.render(template_path("build.html"), **data)


class UpdateHandler(BaseRequestHandler):
    async def get(self):
        data = await load_update()
        self.render(template_path("update.html"), **data)


class DeployHandler(BaseRequestHandler):
    async def get(self):
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            "https://f-droid.org/repo/status/deploy.json",
            connect_timeout=120,
            request_timeout=120,
        )
        last_modified = parse_http_datetime(response.headers["last-modified"])
        d = json.loads(response.body)
        data = {
            "cmd": " ".join(d["commandLine"]),
            "startTime": datetime.datetime.utcfromtimestamp(d["startTimestamp"] / 1000),
            "endTime": datetime.datetime.utcfromtimestamp(d["endTimestamp"] / 1000),
            "data": d,
            "lastModified": last_modified,
        }
        self.render(template_path("deploy.html"), **data)


class DisabledHandler(BaseRequestHandler):
    async def get(self):
        data = await load_update()
        self.render(template_path("disabled.html"), **data)


class AntiFeaturesHandler(BaseRequestHandler):
    async def get(self):
        data = await load_update()

        # calculate number of apps with anti-features
        afset = set()
        for k, v in data["data"]["antiFeatures"].items():
            for afapp in v["apps"]:
                afset.add(afapp)
        data["total_app_count"] = len(afset)
        data["af_names"] = AF_NAMES

        self.render(template_path("anti-features.html"), **data)


class AntiFeatureHandler(BaseRequestHandler):
    async def get(self, anti_feature):
        d = await load_update()
        d["anti_feature"] = anti_feature
        d["anti_feature_name"] = AF_NAMES.get(anti_feature, anti_feature)
        self.render(template_path("anti-feature.html"), **d)


class LogHandler(BaseRequestHandler):
    async def get(self, appid, vercode):
        http_client = tornado.httpclient.AsyncHTTPClient()
        try:
            response = await http_client.fetch(
                "https://f-droid.org/repo/{}_{}.log.gz".format(appid, vercode),
                connect_timeout=120,
                request_timeout=120,
            )
            if response.error:
                raise response.error
        except tornado.httpclient.HTTPClientError as e:
            raise MyException(
                "HTTP 404: Not Found", "no logs available for this build", 404
            ) from e
        else:
            log_content = ""
            if response.headers.get("Content-Encoding", "").startswith("x-gzip"):
                log_content = gzip.decompress(response.body).decode("utf-8")
            else:
                log_content = response.body
            data = {"appid": appid, "vercode": vercode, "log_content": log_content}
            self.render(template_path("log.html"), **data)


def make_app(*, debug: bool):
    settings = {
        "debug": debug,
        "default_handler_class": Err404Handler,
        "template_loader": tornado.template.Loader(
            root_directory=os.path.join(
                os.path.abspath(os.path.dirname(__file__)), "templates"
            )
        ),
        "static_path": os.path.join(
            os.path.abspath(os.path.dirname(__file__)), "static"
        ),
    }
    return tornado.web.Application(
        (
            (r"/", IndexHandler),
            (r"/builds", BuildsHandler),
            (r"/builds/running", RunningHandler),
            (r"/builds/build", BuildHandler),
            (r"/builds/deploy", DeployHandler),
            (r"/builds/update", UpdateHandler),
            (r"/builds/disabled", DisabledHandler),
            (r"/builds/log/([a-zA-Z0-9_\.]+)/([0-9]+)", LogHandler),
            (r"/anti-features", AntiFeaturesHandler),
            (r"/anti-feature/([a-zA-Z0-9_\.]+)", AntiFeatureHandler),
        ),
        **settings,
    )


async def shutdown():
    # http_server.stop()
    # for client in ws_clients.values():
    #     client['handler'].close()
    # await tornado.gen.sleep(1)
    tornado.ioloop.IOLoop.current().stop()


def exit_handler(sig, frame):
    print("shutting down ...")
    tornado.ioloop.IOLoop.instance().add_callback_from_signal(shutdown)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        "-d",
        action="store_true",
        default=False,
        help="If this flag is present, the app will start in debug mode. This is only useful for development, never aktivate this on your servers.",
    )
    parser.add_argument(
        "--port",
        "-p",
        type=int,
        help="set the network port on which this application server is supposed to listen on",
    )
    parser.add_argument(
        "--address",
        "-a",
        type=str,
        help="set the network address on which this application server is supposed to listen on",
    )
    parser.add_argument(
        "--config", "-c", type=str, help="load configuration from this file"
    )
    args = parser.parse_args()

    cfg = fdroidmonitor.cfg.load(args=args)

    if cfg["debug"]:
        print("staring in debug mode ...")
    app = make_app(debug=cfg["debug"])
    app.listen(cfg["port"], cfg["address"])

    signal.signal(signal.SIGTERM, exit_handler)
    signal.signal(signal.SIGINT, exit_handler)

    print("listening for requests on: http://{}:{}".format(cfg["address"], cfg["port"]))
    tornado.ioloop.IOLoop.current().start()
